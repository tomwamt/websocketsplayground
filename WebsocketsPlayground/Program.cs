﻿using System;

namespace WebsocketsPlayground
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 7997;
            if (args.Length > 0)
            {
                port = Int32.Parse(args[0]);
            }

            var server = new ChatServer(port);

            Console.CancelKeyPress += (sender, a) =>
            {
                server.Stop();
                Environment.Exit(0);
            };

            server.Start();
        }
    }
}
