﻿using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace WebsocketsPlayground
{
    class ChatServer
    {
        readonly HttpServer _server;

        public ChatServer(int port)
        {
            _server = new HttpServer(port);
            _server.Log.Level = LogLevel.Info;
            _server.AddWebSocketService<ChatBehavior>("/chat");
        }

        public void Start()
        {
            _server.Start();
            if (_server.IsListening)
            {
                _server.Log.Info($"Server started on port {_server.Port}");

                ConsoleListenLoop();
            }
            else
            {
                throw new ApplicationException("Could not start server");
            }
        }

        public void Stop()
        {
            _server.Stop();
            _server.Log.Info("Server stopped");
        }

        public void Broadcast(string message)
        {
            _server.WebSocketServices.Broadcast($"Server: msg");
        }

        void ConsoleListenLoop()
        {
            while (true)
            {
                Broadcast(Console.ReadLine());
            }
        }
    }

    class ChatBehavior : WebSocketBehavior
    {
        string _name;

        protected override void OnMessage(MessageEventArgs e)
        {
            if (_name is null)
            {
                _name = e.Data;
                Log.Info($"join: {_name} ({ID})");
            }
            else
            {
                string msg = $"{_name}: {e.Data}";
                Log.Info($"msg recv: {msg}");
                Sessions.Broadcast(msg);
            }
        }
    }
}
